﻿///$tab 01 Main
$(Must_Include=..\logistica qvs\02 transformacion\main.qvs);
///$tab 02 Variables
$(Must_Include=..\logistica qvs\02 transformacion\02 variables.qvs);
///$tab Pruebas
///$tab 03 Estado Pedidos por Lineas
$(Must_Include=..\logistica qvs\02 transformacion\03 estado pedidos por lineas.qvs);
///$tab 04 Situaciones de Expedicion
$(Must_Include=..\logistica qvs\02 transformacion\04 situaciones de expedicion.qvs);
///$tab 05 Inconsistencias
$(Must_Include=..\logistica qvs\02 transformacion\05 inconsistencias.qvs);
///$tab 06 Planta Total CEDI
$(Must_Include=..\logistica qvs\02 transformacion\06 planta total cedi.qvs);
///$tab 07 Politicas Proceso Logistico Tipos 1,3,4
$(Must_Include=..\logistica qvs\02 transformacion\07 politicas proceso logistico tipos 1 3 4.qvs);
///$tab 08 Fecha y Hora de Movimientos Tipos 1,3,4
$(Must_Include=..\logistica qvs\02 transformacion\08 fecha y hora de movimientos tipos 1 3 4.qvs);
///$tab 09 Turnos Tipo 1,3,4
$(Must_Include=..\logistica qvs\02 transformacion\09 turnos tipo 1 3 4.qvs);
///$tab 10 Entrada de Pedidos Tipos 1,3,4
$(Must_Include=..\logistica qvs\02 transformacion\10 entrada de pedidos tipos 1 3 4.qvs);
///$tab 11 Entrega Tipos 1,3,4
$(Must_Include=..\logistica qvs\02 transformacion\11 entrega tipos 1 3 4.qvs);
///$tab 12 Entrega Tipo 6,7 Nacional
$(Must_Include=..\logistica qvs\02 transformacion\12 entrega tipo 6 7 nacional.qvs);
///$tab 13 Entregas Tipo 6,7 Local
$(Must_Include=..\logistica qvs\02 transformacion\13 entregas tipo 6 7 local.qvs);
///$tab 14 Entregas Pedidos
$(Must_Include=..\logistica qvs\02 transformacion\14 entregas pedidos.qvs);
///$tab 15 Link Table
$(Must_Include=..\logistica qvs\02 transformacion\15 link table.qvs);
///$tab 16 Calendario
$(Must_Include=..\logistica qvs\02 transformacion\16 calendario.qvs);
///$tab 17 Maestras
$(Must_Include=..\logistica qvs\02 transformacion\17 maestras.qvs);
///$tab >> Salir >>
$(Must_Include=..\logistica qvs\02 transformacion\salir.qvs);
select
/*+ INDEX (TT_COL_PRO.TYT_INCIDENCIAS inc_pk) Index (tyt_incidencias_linea inl_pk) Index (tyt_interlocutores int_pk)Index (TYT_SUBTIPO SBT_PK)
Index (lineas_pedven lpv_pk) Index (destinatarios des_pk) Index (articulos art_pk) Index (TT_COL_PRO.TYT_TIPOS tip_pk) Index (TYT_INCIDENCIAS_ACCIONES ina_pk) */
       inc_codinc,
       inc_codtip,
       tip_nombre,
       inc_codsub,
       sbt_nombre,
       inc_codcli,
       inc_codcen,
       inc_nombre,
       inc_desbre,
       inc_pevein,
       inc_pedido,
       inc_estado,
       inc_fecha,
       ina_fecalt,
       inc_usucre,
       inc_coddes,
       des_razsoc,
       inc_intasg,
       int_nombre,
       inl_codlin,
       lpv_articu,
       art_codext,
       art_denomi,
       lpv_canenv,
       inl_cantid
from   tt_col_pro.tyt_incidencias,
       tt_col_pro.tyt_incidencias_linea,
       tt_col_pro.tyt_interlocutores,
       lineas_pedven,
       destinatarios, 
       articulos,
       tt_col_pro.tyt_tipos,
       tt_col_pro.tyt_subtipo,
       TT_COL_PRO.TYT_INCIDENCIAS_ACCIONES 
where  
       int_codint = inc_intasg
  and  inl_codinc = inc_codinc
  and  lpv_pedido = inc_pevein
  and  lpv_codlin = inl_codlin
  and  lpv_client = inc_codcli
  and  lpv_almace = inc_codcen
  and  des_coddes = inc_coddes
  
  and  art_codart = lpv_articu
  and  art_codalm = lpv_almace
  and  art_codcli = lpv_client
  
  and  tip_codtip = inc_codtip
  and  sbt_codsub = inc_codsub
  and  sbt_codtip = inc_codtip
  and  INC_CODINC = INA_CODINC
  AND INA_DESCRI = 'Cierre de Incidencia'
  AND INA_FECALT  >= to_date(sysdate -1)  AND INA_FECALT < to_date(sysdate);
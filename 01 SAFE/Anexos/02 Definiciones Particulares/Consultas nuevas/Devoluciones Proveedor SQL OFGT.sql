SELECT 
/*+ INDEX  (CABEC_DEVPRO CDP_ALMSITFEC_BU)*/
CDP_CODIGO, CDP_ALMACE, CDP_NUMDOC, CDP_PROPIE, CDP_PROVEE, CDP_FECDEV, CDP_OPEDEV, CDP_TERMIN, CDP_SITCAB, LDP_CODIGO, LDP_CODLIN, LDP_ARTICU, LDP_CANTID, LDP_SITLIN,
LDP_CANREA, LDP_PALETA, LDP_LOTEFA, PRO_RAZSOC, PRO_CODCIF, CDP_ORDCOM, SUM(LDP_VALUNI)
FROM 
CABEC_DEVPRO, LINEAS_DEVPRO, PROVEEDOR
WHERE 
(( PRO_CODIGO = CDP_PROVEE) AND ( CDP_CODIGO = LDP_CODIGO ) ) 
AND CDP_ALMACE = LDP_ALMACE
AND CDP_PROPIE = LDP_PROPIE
AND CDP_FECDEV >= to_date(sysdate -2)  AND CDP_FECDEV < to_date(sysdate)
AND CDP_SITCAB != 'PE'
GROUP BY CDP_CODIGO, CDP_ALMACE, CDP_NUMDOC, CDP_PROPIE, CDP_PROVEE, CDP_FECDEV, CDP_OPEDEV, CDP_TERMIN, CDP_SITCAB, LDP_CODIGO, LDP_CODLIN, LDP_ARTICU, LDP_CANTID, 
LDP_SITLIN, LDP_CANREA, LDP_PALETA, LDP_LOTEFA, PRO_RAZSOC, PRO_CODCIF, CDP_ORDCOM
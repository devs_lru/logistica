 SELECT
 /* + Index (movimientos mvm_pk) Index (paletas pal_pk) Index (articulos art_pk) Index (operarios ope_pk) */ 
          mvm_codigo, mvm_fehofi, mvm_almace, mvm_propie, mvm_articu,
          art_denomi, mvm_tipmov, mvm_operar, ope_nombre, mvm_paleta,
          mvm_numdoc, mvm_cantid, pal_lotefa, pal_feccad, mvm_pasori,
          mvm_colori, mvm_altori, mvm_pasdes, mvm_coldes, mvm_altdes,
          mvm_coment
     FROM movimientos, articulos , operarios, paletas 
    WHERE 
          mvm_tipmov NOT IN ('CC', 'DC', 'CL')
      AND mvm_fehofi >= to_date(sysdate -1)  AND mvm_fehofi < to_date(sysdate)
      AND mvm_propie = pal_propie(+)
      AND mvm_almace = pal_almace(+)
      AND mvm_articu = pal_articu(+)
      AND mvm_paleta = pal_codigo(+)
      AND mvm_propie = art_propie 
      AND mvm_almace = art_almace 
      AND mvm_articu = art_codigo
      AND ope_codigo = mvm_operar
      AND ope_almace = mvm_almace; 
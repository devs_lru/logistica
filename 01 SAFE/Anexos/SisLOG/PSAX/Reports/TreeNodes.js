// You can find instructions for this file here:
// http://www.treeview.net

// Decide if the names are links or just the icons
USETEXTLINKS = 1  //replace 0 with 1 for hyperlinks

// Decide if the tree is to start all open or just showing the root folders
STARTALLOPEN = 0 //replace 0 with 1 to show the whole tree

ICONPATH = 'Support/' //change if the gif's folder is a subfolder, for example: 'images/'


USEICONS = 1

{
foldersTree = gFld("Title", "title.htm")
foldersTree.iconSrc = ICONPATH + "Gif/globe.gif"
Diag_Node = insFld(foldersTree, gFld("SisLOG_PSAX.dm1", "diagram.htm"))
Diag_Node.iconSrc = ICONPATH + "Gif/ERSTUDIO.gif"
Diag_Node.iconSrcClosed = ICONPATH + "Gif/ERSTUDIO.gif"
Model_Node = insFld(Diag_Node, gFld("Logical", "Model1/Model1.htm"))
Model_Node.iconSrc = ICONPATH + "Gif/logical.gif"
Model_Node.iconSrcClosed = ICONPATH + "Gif/logical.gif"
{
Submodel1 = insFld(Model_Node, gFld("Main Model", "Model1/Submodel1.htm"))
Submodel1.iconSrc = ICONPATH + "Gif/logmain.gif"
Submodel1.iconSrcClosed = ICONPATH + "Gif/logmain.gif"
SubmodelImgNode = insDoc(Submodel1, gLnk("R", "Model Image", "Model1/Submodel1_img.htm"))
SubmodelImgNode.iconSrc = ICONPATH + "Gif/image.gif"
SubmodelImgNode.altTxt = "Model Image"
EntityFolder = insFld(Submodel1, gFld("Entities", "Model1/Sub1EntFrame.htm"))
EntityFolder.iconSrc = ICONPATH + "Gif/entfldr.gif"
EntityFolder.iconSrcClosed = ICONPATH + "Gif/entfldr.gif"
AttrFolder = insFld(Submodel1, gFld("Attributes", "Model1/Sub1AttrFrame.htm"))
AttrFolder.iconSrc = ICONPATH + "Gif/attr.gif"
AttrFolder.iconSrcClosed = ICONPATH + "Gif/attr.gif"
IndexFolder = insFld(Submodel1, gFld("Keys", "Model1/Sub1IdxFrame.htm"))
IndexFolder.iconSrc = ICONPATH + "Gif/index.gif"
IndexFolder.iconSrcClosed = ICONPATH + "Gif/index.gif"
RelFolder = insFld(Submodel1, gFld("Relationships", "Model1/Sub1RelFrame.htm"))
RelFolder.iconSrc = ICONPATH + "Gif/relfldr.gif"
RelFolder.iconSrcClosed = ICONPATH + "Gif/relfldr.gif"
}
Model_Node = insFld(Diag_Node, gFld("Physical", "Model2/Model2.htm"))
Model_Node.iconSrc = ICONPATH + "Gif/physical.gif"
Model_Node.iconSrcClosed = ICONPATH + "Gif/physical.gif"
{
Submodel2 = insFld(Model_Node, gFld("Main Model", "Model2/Submodel2.htm"))
Submodel2.iconSrc = ICONPATH + "Gif/grnfldr.gif"
Submodel2.iconSrcClosed = ICONPATH + "Gif/grnfldr.gif"
SubmodelImgNode = insDoc(Submodel2, gLnk("R", "Model Image", "Model2/Submodel2_img.htm"))
SubmodelImgNode.iconSrc = ICONPATH + "Gif/image.gif"
SubmodelImgNode.altTxt = "Model Image"
EntityFolder = insFld(Submodel2, gFld("Tables", "Model2/Sub2EntFrame.htm"))
EntityFolder.iconSrc = ICONPATH + "Gif/entfldr.gif"
EntityFolder.iconSrcClosed = ICONPATH + "Gif/entfldr.gif"
AttrFolder = insFld(Submodel2, gFld("Columns", "Model2/Sub2AttrFrame.htm"))
AttrFolder.iconSrc = ICONPATH + "Gif/attr.gif"
AttrFolder.iconSrcClosed = ICONPATH + "Gif/attr.gif"
IndexFolder = insFld(Submodel2, gFld("Indexes", "Model2/Sub2IdxFrame.htm"))
IndexFolder.iconSrc = ICONPATH + "Gif/index.gif"
IndexFolder.iconSrcClosed = ICONPATH + "Gif/index.gif"
RelFolder = insFld(Submodel2, gFld("Foreign Keys", "Model2/Sub2RelFrame.htm"))
RelFolder.iconSrc = ICONPATH + "Gif/relfldr.gif"
RelFolder.iconSrcClosed = ICONPATH + "Gif/relfldr.gif"
}
{
{
DL_Node = insFld(Diag_Node, gFld("Data Lineage", "javascript:parent.op()"))
DL_Node.iconSrc = ICONPATH + "Gif/data_lineage.gif"
DL_Node.iconSrcClosed = ICONPATH + "Gif/data_lineage.gif"
VDL_Node = insFld(DL_Node, gFld("Data Flows", "javascript:parent.op()"))
VDL_Node.iconSrc = ICONPATH + "Gif/DataFlowsFolder.gif"
VDL_Node.iconSrcClosed = ICONPATH + "Gif/DataFlowsFolder.gif"
DMR_FOLDER = insFld(DL_Node, gFld("Data Movement Rules", "diagram.htm#DMR"))
DMR_FOLDER.iconSrc = ICONPATH + "Gif/data_movement_rule_node.gif"
DMR_FOLDER.iconSrcClosed = ICONPATH + "Gif/data_movement_rule_node.gif"
ST_Node = insFld(DL_Node, gFld("Data Sources", "diagram.htm#STM"))
ST_Node.iconSrc = ICONPATH + "Gif/data_source_target.gif"
ST_Node.iconSrcClosed = ICONPATH + "Gif/data_source_target.gif"
}
}
{
DictObjects1 = insFld(Diag_Node, gFld("SISLOG_PSAX_DD", "javascript:parent.op()"))
DictObjects1.iconSrc = ICONPATH + "Gif/dict.gif"
DictObjects1.iconSrcClosed = ICONPATH + "Gif/dict.gif"
DomainSection1 = insFld(DictObjects1, gFld("Domains", "DataDictionary/DD1DomainFrame.htm"))
DomainSection1.iconSrc = ICONPATH + "Gif/open.gif"
DomainSection1.iconSrcClosed = ICONPATH + "Gif/open.gif"
}
}
